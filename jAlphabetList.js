(function($, undefined){
    'use strict';

    //
    var search_box   = null,
        clear_button = null,
        list_data    = null,
        instance     = null;

    var default_settings = {
        data: []
    };

    var object_cmp = function(f, s) {
        return (f.name < s.name) ? -1 : (f.name > s.name) ? 1 : 0;
    };

    var html_utils = {
        build_ul: function(items, skip_separators){
            var html = [];

            html.push('<ul>');
            $.each(items, function(i, v){
                if (skip_separators) {
                    html.push('<li>' + v + '</li>');
                } else {
                    var id = v.hasOwnProperty('id') ? 'id="' + v.id + '"' : undefined;
                    html.push(id === undefined ? html_utils.insert_separator(v.name) :
                        '<li ' + id + '>' + v.name + '</li>');
                }
            });
            html.push('</ul>');

            return html.join('\n');
        },
        search_box: function(){
            return [
                '<div class="jsl-search-box">',
                    '<input type="text" placeholder="Search..." />',
                    '<div class="btn-clear">Clear</div>',
                '</div>'
            ].join('\n');
        },
        insert_separator: function(title){
            return '<li class="separator" id="jsl-anchor-' + title + '" >' + title + '</li>';
        },
        alphabet_index: function(index){
            var html = [];

            html.push('<div class="jsl-alphabet-index">');
                html.push(this.build_ul(index, true));
            html.push('</div>');

            return html.join('\n');
        },
        main_list: function(list){
            var html = [];
            var prepared_list = [];

            list.sort(object_cmp);

            var last_letter = '';
            $.each(list, function(i, v){
                var first_char = v.name.charAt(0);
                if (last_letter !== first_char) {
                    last_letter = first_char;
                    prepared_list.push({name: first_char });
                }
                prepared_list.push({name: v.name, id: v.id});
            });
            prepared_list.sort(object_cmp);

            html.push('<div class="jsl-main">');
                html.push(this.build_ul(prepared_list));
            html.push('</div>');

            return html.join('\n');
        }
    };

    var uniqueArray = function(arr) {
        return arr.reduce(function(p, c) {
            if (p.indexOf(c) < 0) p.push(c);
            return p;
        }, []);
    };

    //

    var getAlphabetIndex = function(data){
        return uniqueArray($.map(data, function(val){
            return val.name.charAt(0).toUpperCase();
        })).sort();
    };

    var constructList = function(options){
        instance  = this;
        list_data = options.data;

        // construct html
        var index = getAlphabetIndex(list_data);
        var common_html = [];

        instance.addClass('jsl-wrapper');
        common_html.push(html_utils.search_box());
        common_html.push('<div class="jsl-content">');
            common_html.push(html_utils.alphabet_index(index));
            common_html.push(html_utils.main_list(list_data));
        common_html.push('</div>');

        instance.html(common_html.join('\n'));

        // bind actions
        search_box = $('.jsl-search-box input');
        search_box.on('keyup', live_search);

        clear_button = $('.jsl-search-box div.btn-clear');
        clear_button.on('click', function(){
            if (search_box.val() !== '') {
                search_box.val('');
                search_box.keyup();
            }
        });

        // raise index click
        $('.jsl-alphabet-index ul > li').on('click', function(e){
            e.preventDefault();
            var anchor = '#jsl-anchor-' + $(this).text();
            if (!$(anchor).length) return; // check existing div

            var position = $(anchor).parent().scrollTop() + $(anchor).offset().top - $(anchor).parent().offset().top;
            $('.jsl-main').animate({ scrollTop: position }, 'slow');

            // raise on index change event
            instance.trigger('index_changed.jsl');
        });

        // raise click node click event
        $('.jsl-content').on('click', '.jsl-main ul > li:not(.separator)', function(e){
            e.preventDefault();
            instance.trigger('changed.jsl', [this]);
        });

        adapt_height();
    };

    var adapt_height = function(){
        var h = $('.jsl-alphabet-index ul').height() - 1;
        $('.jsl-main').css({
            'max-height': h,
            'min-height': h,
            'height'    : h
        });
    };

    var update_main_list = function(new_data){
        var html = html_utils.main_list(new_data);
        $('div.jsl-main').replaceWith(html);
        adapt_height();
    };

    var live_search = function() {
        var results = [];
        var query = search_box.val();
        var re = new RegExp('^' + query, 'i');

        $.each(list_data, function(i, v){
            if (re.test(v.name)){
                results.push(v);
            }
        });

        update_main_list(results);
    };

    var methods = {
        init: function(params){
            var options = $.extend({}, default_settings, params);
            constructList.apply(this, [options]);
            return this;
        },
        destroy: function(){
            // todo
            console.info('Alphabet list destroyed');
        }
    };

    $.fn.jAlphabetList = function(method){
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if ($.isPlainObject(method) || !method){
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' not found');
        }
    };

})(jQuery);